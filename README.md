# 2018 IML challenge 

## description

The challenge detail are decribed in the wiki.

## submission

Participants need to submit the prediction as numpy array or root tree for the
qcd_test dataset at the 10th of April before 24:00 CERN time. If you have
access to the CERN AFS filesystem (e.g. as CERN account holder), please upload
the file to

```
/afs/cern.ch/work/r/rhaake/IML_Challenge/
```

and send a mail to inform the IML coordinators using the subject 'IML challenge
contribution'. If you are not able to upload the file, you can directly send a
link to the file (e.g. a dropbox link) to the IML coordinators.

## public solutions

 - [Mykhailo Lisovyi's solution on github](https://github.com/mlisovyi/IML_challenge_2018) (comparing XGBoost vs LightGBM).
 - [Danilo Enoque Ferreira De Lima's solution on github](https://github.com/daniloefl/IML_challenge_2018/blob/master/Tensorflow%20Playground.ipynb) (tensorflow).
